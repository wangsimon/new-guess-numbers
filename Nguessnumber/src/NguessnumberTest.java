import java.util.Scanner;

public class NguessnumberTest {
    public static void main(String[] args) {
        int i=7;//中共的次数
        Nguessnumber.generateAnswer g = new Nguessnumber.generateAnswer();
        System.out.println("本次游戏共有"+i+"次机会");
        System.out.println("请输入4个0-9之间的不重复的整型数");
        Scanner scanner = new Scanner(System.in);
        while (i>0) {
            System.out.println("还有第"+i+"次机会");
            String r = scanner.next();
            if ("exit".equals(r))
                break;
            if (!g.assertEquals(r)) {
                System.out.println("输入4位数字, 且不能重复");
                continue;
            }
               String result = g.compareGuessAnswer(r);
                     if ("4A0B".equals(result)) {
                    System.out.println("恭喜, 猜对了");
                  System.out.println("正确答案为:"+g);
                         System.out.println("请问是否要再进行一局(继续(请输入1)/退出(请输入0))?");
                         scanner= new Scanner(System.in);
                         int sc = scanner.nextInt();
                         if (sc == 1) {
                             //重置游戏为初始值
                             i = 7;
                             g = new Nguessnumber.generateAnswer();
                             System.out.println("继续游戏！！本次游戏共有"+i+"次机会");
                             System.out.println("请猜一个1-100之间的整数：");
                             continue;
                         }else if (sc==0){
                             System.out.println("您已退出游戏！！！");
                             return;
                         }

                  break;
            } else {
                System.out.println(result);
            }
            i--;
            //如果次数用完反馈"游戏结束,你没有猜对"
            if (i==0) {
                System.out.println("很抱歉,你没有猜对");
                System.out.println("正确答案为:"+g);
                System.out.println("请问是否要再进行一局(继续(请输入1)/退出(请输入0))?");
                scanner= new Scanner(System.in);
                int sc = scanner.nextInt();
                if (sc == 1) {
                    //重置游戏为初始值
                    i = 7;
                    g = new Nguessnumber.generateAnswer();
                    System.out.println("继续游戏！！本次游戏共有"+i+"次机会");
                    System.out.println("请猜一个1-100之间的整数：");
                    continue;
                }else if (sc==0){
                    System.out.println("您已退出游戏！！！");
                    return;
                }
            }
        }
        System.out.println("正确答案为:"+g);
    }

}
