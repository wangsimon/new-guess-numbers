import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Nguessnumber {
        //创建generateAnswer方法，随机生成4个0-9之间的不重复的整型数作为答案并返回的功能。
    public static class generateAnswer {
         int[] nums = new int[4];
        public String toString() {
            StringBuffer result = new StringBuffer("");
            for (int i : nums) {
                result.append(i + " ");
            }
            return result.toString();
        }

        public generateAnswer() {
            List<Integer> list = new ArrayList<Integer>();
            for (int i = 0; i < 10; i++) {
                list.add(i);
            }
            Random r = new Random();
            for (int i = 0; i < 4; i++) {
                int k = r.nextInt(10 - i);
                nums[i] = list.get(k);
                list.remove(k);
            }
        }
                //判断输入的数字的数量是否为4
            public boolean assertEquals(String r) {
                if (r.length() != 4) {
                    return false;
                }
                if (!r.matches("\\d{4}")) {
                    return false;
                }
                for (int i = 0; i < 4; i++) {
                    if (r.lastIndexOf(r.charAt(i)) > i) {
                        return false;
                    }
                }
                return true;
            }
            //判断用户输入的数字存在且位置的数字个数
            private String A(String r) {
                int count = 0;
                for (int i = 0; i < r.length(); i++) {
                    if (nums[i] == Integer.parseInt(r.charAt(i) + "")) {
                        count++;
                    }
                }
                return count + "A";
            }
            //判断用户输入数字存在但位置不正确的个数
            private String B(String r) {
                int count = 0;
                for (int i = 0; i < r.length(); i++) {
                    int temp = Integer.parseInt(r.charAt(i) + "");
                    for (int j = 0; j < r.length(); j++) {
                        if (nums[j] == temp && i != j)
                            count++;
                    }
                }
                return count + "B";
            }
            public String compareGuessAnswer(String r) {
            System.out.println("A表示存在且位置正确的数字个数,B表示存在但位置不正确的个数");
            System.out.println(" 如2A2B,表示用户猜的数字位置正确有两个,不正确的位置有两个");
            return A(r)+B(r);
            }
        }
}